<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Todo extends Model
{
    protected $table = 'todo_list';

    public static function get_todos($all=true, $limit = 0, $offset = 0)
    {
        if($all)
        {
            return DB::table('todo_list')->get();
        }
        else
        {
            return DB::table('todo_list')->skip($offset)->take($limit)->get();
        }
    }
}
