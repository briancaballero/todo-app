<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Todo;
use App\Logs;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $todos = Todo::get_todos();
        return view('home')->with('todos', $todos);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'description' => 'required'
        ]);

        $name = $request->input('name');
        $description = $request->input('description');

        $todo = new Todo;
        $todo->userid = Auth::user()->id;
        $todo->name = $name;
        $todo->description = $description;
        $todo->complete = 0;
        $todo->save();

        $log = new Logs;
        $log->userid = Auth::user()->id;
        $log->todoid = $todo->id;
        $log->description = 'New todo was added';
        $log->data = json_encode($todo);
        $log->save();

        return redirect()->route('home')->with('message', 'Todo successfully added!');
    }

    public function remove(Request $request, $id)
    {
        $todo = Todo::find($id);
        $todo->delete();

        $log = new Logs;
        $log->userid = Auth::user()->id;
        $log->todoid = $id;
        $log->description = 'Todo was deleted';
        $log->data = json_encode(array('todoid' => $id));
        $log->save();

        return redirect()->route('home')->with('message', 'Todo successfully deleted!');
    }

    public function bulk(Request $request) {
        $validatedData = $request->validate([
            'todo-ids' => 'required',
            'action' => 'required'
        ]);
        
        $ids = $request->input('todo-ids');
        $action = $request->input('action');

        

        $ids = explode(',',$ids);

        foreach($ids as $id) {

            $todo = Todo::find($id);

            if($action == 'complete') {
                $todo->complete = 1;
                $todo->save();
            } elseif($action == 'delete') {
                $todo->delete();
            }
        }

        $log = new Logs;
        $log->userid = Auth::user()->id;
        $log->todoid = 0;
        $log->description = 'Todo(s) was '.$action.'d!';
        $log->data = json_encode(array(
                'todoids' => $request->input('todo-ids'),
                'action' => $action
            ));
        $log->save();

        return redirect()->route('home')->with('message', 'Todo(s) successfully '.$action.'d!');
    }
}
