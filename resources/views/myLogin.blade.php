<!DOCTYPE html>
@extends('theme.splash')

@section('content')

<div class="login-wrap">
    <div class="login-content">
        <div class="login-logo">
            <a href="#">
                <img src="{!! asset('theme/images/todo.jpg') !!}" alt="CoolAdmin">
            </a>
        </div>
        
        <div class="login-form">
            <form action="" method="post">
                @csrf
                <div class="@error('email') has-warning @enderror form-group">
                    <label>{{ __('E-Mail Address') }}</label>
                    <input class="au-input au-input--full @error('email') is-invalid form-control @enderror" type="email" name="email" value="{{ old('email') }}" placeholder="{{ __('Email') }}">
                    @error('email')<small class="help-block form-text">{{ $message }}</small>@enderror
                </div>
                <div class="@error('password') has-warning @enderror form-group">
                    <label>{{ __('Password') }}</label>
                    <input class="au-input au-input--full @error('password') is-invalid form-control @enderror" type="password" name="password" placeholder="{{ __('Password') }}">
                    @error('password')<small class="help-block form-text">{{ $message }}</small>@enderror
                </div>
                <div class="login-checkbox">
                    <label>
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>{{ __('Remember Me') }}
                    </label>
                    <!-- <label>
                    <a href="#">Forgotten Password?</a>
                    </label> -->
                </div>
                <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">{{ __('Login') }}</button>
            </form>
        </div>
        
    </div>
</div>

@endsection
