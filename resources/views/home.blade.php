<!DOCTYPE html>
@extends('theme.main')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12" id="todo-list">
            <!-- DATA TABLE -->
            <div class="table-data__tool">
                <div class="table-data__tool-right">
                    <button class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#largeModal">
                        <i class="zmdi zmdi-plus"></i>{{ __('New Todo') }}</button>
                    <button class="au-btn au-btn-icon au-btn--blue au-btn--small" v-on:click="performAction('complete')">
                        {{ __('Mark as Complete') }}</button>
                    <button class="au-btn au-btn-icon au-btn--blue au-btn--small" v-on:click="performAction('delete')">
                        {{ __('Delete') }}</button>
                </div>
            </div>
            <div class="table-responsive table-responsive-data2">
                <table class="table table-data2">
                    <thead>
                        <tr>
                            <th>
                                <label class="au-checkbox">
                                    <input type="checkbox" v-on:click="toggleId($event)">
                                    <span class="au-checkmark"></span>
                                </label>
                            </th>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Description') }}</th>
                            <th>{{ __('Status') }}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($todos as $todo)
                        <tr class="tr-shadow">
                            <td>
                                <label class="au-checkbox">
                                    <input type="checkbox" class="todo-id" value="{{ $todo->id }}" v-model="ids">
                                    <span class="au-checkmark"></span>
                                </label>
                            </td>
                            <td>{{ $todo->name }}</td>
                            <td class="desc">{{ $todo->description }}</td>
                            <td><span class="status--denied">{{ $todo->complete }}</span></td>
                            <td>
                                <div class="table-data-feature">
                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Delete" onclick="event.preventDefault(); if(confirm('Do you want to delete this Todo record?')){document.getElementById('delete-todo-{{ $todo->id }}').submit();}">
                                        <i class="zmdi zmdi-delete"></i>
                                    </button>

                                    <form id="delete-todo-{{ $todo->id }}" action="{{ route('delete',$todo->id) }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </td>
                        </tr>
                        <tr class="spacer"></tr>
                        @empty
                        <tr><td colspan="4">{{ __('No Records found') }}</td></tr>
                        @endforelse
                        
                    </tbody>
                </table>

                <form ref="formPerformAction" id="perform-action" action="{{ route('bulk') }}" method="POST" style="display: none;">
                    @csrf
                    <input type="text" name="todo-ids" v-model="todoids">
                    <input type="text" name="action" v-model="bulkAction">
                </form>
            </div>
            <!-- END DATA TABLE -->
        </div>
    </div>
</div>
@endsection