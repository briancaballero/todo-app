<header class="header-mobile d-block d-lg-none">
                <div class="header-mobile__bar">
                    <div class="container-fluid">
                        <div class="header-mobile-inner">
                            <a class="logo" href="/home">
                                <img src="{!! asset('theme/images/icon/logo.png') !!}" alt="{{ config('app.name', 'Todo') }}" />
                            </a>
                            <button class="hamburger hamburger--slider" type="button">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <nav class="navbar-mobile">
                    <div class="container-fluid">
                        <ul class="navbar-mobile__list list-unstyled">
                            <li class="active">
                                <a href="/home">
                                    <i class="fas fa-tachometer-alt"></i>{{ __('List') }}</a>
                            </li>
                            <li class="active">
                                <a href="/logout" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                    <i class="zmdi zmdi-power"></i>{{ __('Logout') }}</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>