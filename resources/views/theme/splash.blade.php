<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('theme.head')
    <body class="animsition">
        <div class="page-wrapper">
            <div class="page-content--bge5">
                <div class="container">
                @yield('content')
                    
                </div>
            </div>
        </div>
        @include('theme.footer-scripts')
    </body>
</html>