<aside class="menu-sidebar d-none d-lg-block">
    <div class="logo">
        <a href="/home">
            <img src="{!! asset('theme/images/icon/logo.png') !!}" alt="{{ config('app.name', 'Todo') }}" />
        </a>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <ul class="list-unstyled navbar__list">
                <li class="active">
                    <a href="/home">
                        <i class="fas fa-tachometer-alt"></i>{{ __('List') }}</a>
                </li>
                <li class="active">
                    <a href="/logout" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                        <i class="zmdi zmdi-power"></i>{{ __('Logout') }}</a>
                </li>
            </ul>
        </nav>
    </div>
</aside>