<!DOCTYPE html>
@extends('theme.splash')

@section('content')
<div class="main-content">
    <div class="row">
        <div class="card" style="margin: 0 auto;" >
            <img class="card-img-top" src="{!! asset('theme/images/todo.jpg') !!}" alt="Todo">
            <div class="card-body">
                <h4 class="card-title mb-3">Todo List</h4>
                <p class="card-text">App for logging and tracking of project tasks.  Click <a href="/login">here</a> to continue.
                </p>
            </div>
        </div>
    </div>
    
</div>
@endsection
