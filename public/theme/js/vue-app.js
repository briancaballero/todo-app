var todoList = new Vue({
    el: '#todo-list',
    data: {
      ids: [],
      bulkAction: "complete",
      todoids: ""
    },
    methods: {
        toggleId: function(event) {
            var tl = this;
            
            $('.todo-id').each(function(idx, el) {
                $(el).prop('checked', $(event.target).is(':checked'));
            });

            tl.ids = [];
            $('.todo-id').each(function(idx, el){
                if($(el).is(':checked')) {
                    tl.ids.push($(el).val());
                }
            })
        },

        performAction: function(action) {
            if(this.ids.length) {
                this.bulkAction = action;
                this.todoids = this.ids.join(','); console.log(this.todoids);
                $(this.$refs.formPerformAction).find('input[name="todo-ids"]').val(this.todoids);
                $(this.$refs.formPerformAction).find('input[name="action"]').val(this.bulkAction);
                this.$refs.formPerformAction.submit();
            }
        }
    }
});