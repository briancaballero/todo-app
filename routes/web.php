<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginPage')->name('login');
Route::post('login', 'Auth\LoginController@loginNameOrEmail');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::post('add', 'HomeController@store')->name('add');
    Route::post('delete/{id}', 'HomeController@remove')->name('delete');
    Route::post('bulk', 'HomeController@bulk')->name('bulk');
});